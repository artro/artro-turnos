const colors = require('@tailwindcss/postcss7-compat/colors')

module.exports = {
  mode: 'jit',
  purge: [
    './index.html',
    './src/**/*.{vue,js,ts,jsx,tsx,txt}',
    './src/assets/safelist.txt'
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    screens: {
      xs: '375px',
      mini: { raw: '(max-height: 600px)' },
      sm: '640px',
      md: '768px',
      '900px': '900px',
      '450px': '450px',
      '1320px': '1320px',
      lg: '1024px',
      xl: '1280px',
      '2xl': '1536px',
      '3xl': '1750px'
    },
    extend: {
      fontFamily: {
        inter: ['Inter', 'sans-serif']
        // trade: ['Trade Gothic LT Std']
      },
      colors: {
        'normal-text': '#595959',
        spacerLight: '#E5E0EB',
        black: {
          default: '#1A141F'
        },
        gray: {
          default: '#ABA7AF',
          reserved: '#C5C5C5',
          spacer_light: '#E5E0EB',
          light2: '#FCFCFC',
          border: '#CCCCCE',
          card_subtitle_active: '#A1A1A1',
          dark: '#464646',
          dark2: '#4B4B4B',
          title: '#54555A',
          title2: '#595959'
        },
        red: {
          error: '#B64D34'
        },
        green: {
          default: '#00750C',
          success: {
            dark: '#459A33',
            light: '#F0F9E9'
          }
        },
        backgroundSpecialitys: {
          traumatologia: '#E9F3F9',
          aptofisico: '#FDECEE',
          medicinadeldeporte: '#FFF8E8',
          kinesiologia: '#F8F1F5',
          nutricion: '#E7F7F7',
          diagnosticoporimagenes: '#F2FBFF',
          analisisdelapisada: '#FFE8F3',
          reumatologia: '#F3FFDF',
          norelation: '#EEEEEF'
        },
        service: {
          'analisis-de-la-pisada': {
            default: '#E84290',
            bg: '#FFE8F3'
          },
          'apto-fisico': {
            default: '#E83B4D',
            bg: '#FDECEE'
          },
          deportologia: {
            default: '#F8AE04',
            bg: '#FFF8E8'
          },
          'diagnostico-por-imagenes': {
            default: '#3EBAFF',
            bg: '#F2FBFF'
          },
          kinesiologia: {
            default: '#B36C94',
            bg: '#F8F1F5'
          },
          nutricion: {
            default: '#0EABA8',
            bg: '#E7F7F7'
          },
          'otros-servicios': {
            default: '#A09F95',
            bg: '#f3f3f2'
          },
          reumatologia: {
            default: '#9A9B79',
            bg: '#F3FFDF'
          },
          traumatologia: {
            default: '#006EB7',
            bg: '#E9F3F9'
          },
          undefined: {
            default: '#EDEDED'
          }
        }
      },

      boxShadow: {
        soft: '0px 2px 2px rgba(0, 0, 0, 0.15);',
        medium: '0px 4px 8px rgba(0, 0, 0, 0.15);',
        strong: '0px 8px 16px rgba(0, 0, 0, 0.2);'
      }
    }
  },
  variants: {
    extend: {}
  },
  plugins: []
}
