declare global {
  namespace Settings {
    let MOCKING: boolean
  }

  export interface Patient extends EntityWithID {
    id: string
    name: string
    document: string
    birthDate: Date
    affiliateNumber: string
    insurance: Insurance | null
  }

  export interface ExternalPatient extends EntityWithID {
    id: string
    name: string
    document: string
    birthDate: Date
  }

  export interface Insurance extends EntityWithID {
    id: string
    name: string
  }

  export interface Service extends EntityWithID {
    id: string
    name: string
    icon?: string
    color: string
  }

  export interface ClinicLocation extends EntityWithID {
    id: string
    name: string
    address?: string
    iframeLink?: string
  }

  export interface Subservice extends EntityWithID {
    id: string
    name: string
    service: Service
  }

  export interface Professional extends EntityWithID {
    id: string
    name: string
    service: Service
    subservices: Subservice[]
    locations: ClinicLocation[]
    availableDates: AvailableDate[]
  }

  export interface AvailableDate {
    date: Util.SimpleDate
    times: Util.SimpleTime[]
    subservice: Subservice
    location: ClinicLocation
  }

  export interface Appointment
    extends EntityWithID,
      Component.PartialAppointment {
    id: string
    patient: Patient
    subservice: Subservice | null
    location: ClinicLocation
    date: Util.SimpleDate
    time: Util.SimpleTime
    professional: Professional
    confirmationDate: Date
  }

  export interface UserProfile {
    name: string
    address: string
    city: string
    province: string
    refererSource: string
    secondaryEmail: string
    picture: string
  }

  export interface User {
    id: string
    email: string
    profile: UserProfile
    patient: Patient
  }

  namespace Util {
    export interface InfoGroup {
      label: string
      value: string
    }

    export interface EntityWithID {
      id: string
    }

    export type Indexed<T extends EntityWithID> = Map<string, T>

    export interface IdentifiedValue extends Util.EntityWithID {
      id: string
      value: string
    }

    export interface SimpleDate {
      year: number
      month: number
      day: number
    }

    export interface SimpleTime {
      hours: number
      minutes: number
    }

    export interface Field<T> {
      value: T
      error?: string
    }
  }

  namespace Slot {
    export type TimeStatus = 'available' | 'selected' | 'unavailable'
    export interface Time {
      time: Util.SimpleTime
      status: TimeStatus
    }
  }

  namespace Component {
    export type ToastType = 'success' | 'error' | 'info'
    export interface Toast extends Util.EntityWithID {
      id: string
      type: ToastType
      title: string
      body: string
      time?: number // in ms
    }

    export interface PartialAppointment {
      patient: Patient | null
      subservice: Subservice | null
      location: ClinicLocation | null
      date: Util.SimpleDate | null
      time: Util.SimpleTime | null
      professional: Professional | null
    }
  }
}

export {}
