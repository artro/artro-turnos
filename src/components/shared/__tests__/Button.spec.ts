import { shallowMount } from '@vue/test-utils'
import Button from '@/components/shared/Button.vue'

interface Props {
  type?: 'primary' | 'secondary' | 'back'
  label?: string
  submit?: boolean
  disabled?: boolean
  width?: string
}

describe('Component: components/shared/Button.vue', () => {
  const createComponent = (props?: Props) =>
    shallowMount(Button, {
      props: {
        type: 'primary',
        disabled: false,
        ...props
      }
    })

  it('should render properly', () => {
    const wrapper = createComponent()
    expect(wrapper.exists()).toBeTruthy()
  })
})
