import { shallowMount, flushPromises } from '@vue/test-utils'
import router from '@/router'
import { appointment as mock } from '@/utils/mock'
import AppointmentBox from '../AppointmentBox.vue'

interface Props {
  appointment: Appointment | Component.PartialAppointment | null
  showBottom?: boolean
  showTags?: boolean
}

describe('Component: stepper/card/Card.vue', () => {
  const createComponent = (props: Props) =>
    shallowMount(AppointmentBox, {
      props,
      global: {
        plugins: [router]
      }
    })

  it('should render properly', () => {
    const wrapper = createComponent({
      appointment: mock
    })

    expect(wrapper.exists()).toBeTruthy()
  })

  // == SERVICE LINE

  describe('Service Line', () => {
    it('should render', () => {
      const wrapper = createComponent({
        appointment: mock
      })

      const lineContainer = wrapper.find('[data-test=service-line]')

      expect(lineContainer.exists()).toBeTruthy()
    })

    it('should be service color', () => {
      const wrapper = createComponent({
        appointment: mock
      })

      const serviceName = mock.subservice?.service.id
      const expectedClass = `bg-service-${serviceName}-default`

      const lineClasses = wrapper.find('[data-test=service-line]').classes()

      expect(lineClasses).contains(expectedClass)
    })

    it('should be gray', () => {
      const wrapper = createComponent({
        appointment: { ...mock, subservice: null }
      })

      const lineClasses = wrapper.find('[data-test=service-line]').classes()

      expect(lineClasses).contains('bg-service-undefined-default')
    })
  })

  // == TAGS

  describe('Tags', () => {
    it('should render', () => {
      const wrapper = createComponent({
        appointment: mock,
        showTags: true
      })

      const tagsContainer = wrapper.find('[data-test=tags]')

      expect(tagsContainer.exists()).toBeTruthy()
    })

    it('should not render', () => {
      const wrapper = createComponent({
        appointment: mock
      })

      const tagsContainer = wrapper.find('[data-test=tags]')

      expect(tagsContainer.exists()).toBeFalsy()
    })

    it('should show service tag', () => {
      const wrapper = createComponent({
        appointment: mock,
        showTags: true
      })

      const tagsContainer = wrapper.find('[data-test=tags]')
      const serviceTag = tagsContainer.find('[data-test=service-tag]')

      expect(serviceTag.exists()).toBeTruthy()
      expect(serviceTag.text()).toBe(mock.subservice?.service.name)
    })

    it('should not show service tag', () => {
      const wrapper = createComponent({
        appointment: { ...mock, subservice: null },
        showTags: true
      })

      const tagsContainer = wrapper.find('[data-test=tags]')
      const serviceTag = tagsContainer.find('[data-test=service-tag]')

      expect(serviceTag.exists()).toBeFalsy()
    })

    it.todo('should show reserved status')
    it.todo('should show canceled status')
    it.todo('should show terminated status')
  })

  // == BOTTOM

  describe('Bottom', () => {
    it('should render', () => {
      const wrapper = createComponent({
        appointment: mock,
        showBottom: true
      })

      const bottomContainer = wrapper.find('[data-test=bottom]')

      expect(bottomContainer.exists()).toBeTruthy()
    })

    it('should not render', () => {
      const wrapper = createComponent({
        appointment: mock
      })

      const bottomContainer = wrapper.find('[data-test=bottom]')

      expect(bottomContainer.exists()).toBeFalsy()
    })

    // TODO: should be replaced with functionality test
    it('should render share button', () => {
      const wrapper = createComponent({
        appointment: mock,
        showBottom: true
      })

      const bottomContainer = wrapper.find('[data-test=bottom]')
      const shareButton = bottomContainer.find('[data-test=share-btn]')

      expect(shareButton.exists()).toBeTruthy()
    })

    it('should redirect to appointment view', async () => {
      const wrapper = createComponent({
        appointment: mock,
        showBottom: true
      })

      const bottomContainer = wrapper.find('[data-test=bottom]')
      const viewAppointmentButton = bottomContainer.find(
        '[data-test=view-appointment-btn]'
      )

      const push = vi.spyOn(router, 'push')
      await viewAppointmentButton.trigger('click')

      const pushCall = push.mock.calls[0][0] // First call, first param

      expect(pushCall).toHaveProperty('name')
      expect((pushCall as any).name).toBe('Appointment')
    })
  })
})
