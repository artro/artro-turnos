import { nextTick } from 'vue'
import { mount } from '@vue/test-utils'
import Timer from '../card/views/Timer.vue'

interface TimerProps {
  seconds: number
}

describe.skip('Component: stepper/card/Timer', () => {
  const createComponent = (props: TimerProps) => mount(Timer as any, { props })

  it('should render properly', () => {
    const wrapper = createComponent({ seconds: 0 })
    expect(wrapper.exists()).toBeTruthy()
  })

  it('should emit finish', () => {
    const wrapper = createComponent({ seconds: 0 })
    const emittedEvents = Object.keys(wrapper.emitted())
    expect(emittedEvents.includes('finish')).toBeTruthy()
  })

  it('should not emit finish', () => {
    const wrapper = createComponent({ seconds: 1 })
    const emittedEvents = Object.keys(wrapper.emitted())
    expect(emittedEvents.includes('finish')).toBeFalsy()
  })

  it('should render a coherent message', async () => {
    const PROPS_TO_MESSAGE = [
      { props: { seconds: 3 }, message: '3 segundos ' },
      { props: { seconds: 1 }, message: '1 segundo ' },
      { props: { seconds: 60 }, message: '1 minuto ' },
      { props: { seconds: 300 }, message: '5 minutos ' }
    ]

    for (const ptm of PROPS_TO_MESSAGE) {
      const wrapper = createComponent(ptm.props)

      await nextTick()
      const message = wrapper.find('[data-test=timer-message]').text()

      expect(message.includes(ptm.message)).toBeTruthy()
    }
  })

  it('should format clock properly', async () => {
    const PROPS_TO_CLOCK = [
      { props: { seconds: 3 }, clock: '00:03' },
      { props: { seconds: 23 }, clock: '00:23' },
      { props: { seconds: 72 }, clock: '01:12' },
      { props: { seconds: 671 }, clock: '11:11' }
    ]

    for (const ptc of PROPS_TO_CLOCK) {
      const wrapper = createComponent(ptc.props)

      await nextTick()
      const clock = wrapper.find('[data-test=timer-clock]').text()

      expect(clock).toEqual(ptc.clock)
    }
  })

  it('should render an error', async () => {
    const wrapper = createComponent({ seconds: -1 })

    await nextTick()

    const error = wrapper.find('[data-test=active-error]')
    expect(error.exists()).toBeTruthy()
  })

  it('should decrement seconds every second', async () => {
    const wrapper = createComponent({ seconds: 5 })

    await nextTick()
    const getClock = () => wrapper.find('[data-test=timer-clock]').text()

    expect(getClock()).toEqual('00:05')

    // Malas prácticas = eficiencia
    // @ts-ignore
    await wrapper.vm.handleInterval()

    expect(getClock()).toEqual('00:04')
  })

  it('should decrement minutes properly', async () => {
    const wrapper = createComponent({ seconds: 60 })
    await nextTick()
    const getClock = () => wrapper.find('[data-test=timer-clock]').text()

    expect(getClock()).toEqual('01:00')

    // Malas prácticas = eficiencia
    // @ts-ignore
    await wrapper.vm.handleInterval()

    expect(getClock()).toEqual('00:59')
  })
})
