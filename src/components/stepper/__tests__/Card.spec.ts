import { nextTick } from 'vue'
import { shallowMount, mount } from '@vue/test-utils'
import Card from '../card/Card.vue'

type CardFieldProps = any

interface CardProps {
  index: number
  currentStep: number
  title: string
  view: 'fields' | 'timer'
  fields?: CardFieldProps[]
  secondsLeft?: number
}

describe.skip('Component: stepper/card/Card.vue', () => {
  const createComponent = (props: CardProps) =>
    shallowMount(Card as any, { props })

  const createComponentFull = (props: CardProps) =>
    mount(Card as any, { props })

  it('should render properly', () => {
    const wrapper = createComponent({
      index: 0,
      currentStep: 0,
      title: 'Test Card',
      view: 'timer',
      secondsLeft: 0
    })

    expect(wrapper.exists()).toBeTruthy()
  })

  it('should be active', () => {
    const wrapper = createComponent({
      index: 0,
      currentStep: 0,
      title: 'Test Card',
      view: 'timer',
      secondsLeft: 0
    })

    expect(wrapper.attributes('data-test')).toBe('active-card')
  })

  it('should be inactive', () => {
    const diffCurrentStep = createComponent({
      index: 0,
      currentStep: 2,
      title: 'Test Card',
      view: 'timer',
      secondsLeft: 0
    })

    const diffIndex = createComponent({
      index: 1,
      currentStep: 0,
      title: 'Test Card',
      view: 'timer',
      secondsLeft: 0
    })

    expect(diffCurrentStep.attributes('data-test')).toBe('inactive-card')
    expect(diffIndex.attributes('data-test')).toBe('inactive-card')
  })

  it('should not render modify button', () => {
    const timerView = createComponentFull({
      index: 0,
      currentStep: 1,
      title: 'Test Card',
      view: 'timer',
      secondsLeft: 0
    })

    const timerBtn = timerView.find('[data-test=modify-button]')
    expect(timerBtn.isVisible()).toBeFalsy()

    const activeCard = createComponentFull({
      index: 1,
      currentStep: 1,
      title: 'Test Card',
      view: 'fields'
    })

    const activeBtn = activeCard.find('[data-test=modify-button]')
    expect(activeBtn.isVisible()).toBeFalsy()

    const nextStep = createComponentFull({
      index: 3,
      currentStep: 1,
      title: 'Test Card',
      view: 'fields'
    })

    const nextBtn = nextStep.find('[data-test=modify-button]')
    expect(nextBtn.isVisible()).toBeFalsy()
  })

  it('should render modify button', () => {
    const pastStep = createComponentFull({
      index: 0,
      currentStep: 1,
      title: 'Test Card',
      view: 'fields'
    })

    const pastBtn = pastStep.find('[data-test=modify-button]')
    expect(pastBtn.isVisible()).toBeTruthy()
  })

  it('should emit goto-step', async () => {
    const CARD_INDEX = 0
    const wrapper = createComponentFull({
      index: CARD_INDEX,
      currentStep: 1,
      title: 'Test Card',
      view: 'fields'
    })

    const modifyBtn = wrapper.find('[data-test=modify-button]')
    await modifyBtn.trigger('click')
    const emmitedIndex = wrapper.emitted()['goto-step'][0]

    expect(emmitedIndex).toEqual([CARD_INDEX])
  })

  it('should emit finish-counter', async () => {
    const CARD_INDEX = 0
    const wrapper = createComponentFull({
      index: CARD_INDEX,
      currentStep: 0,
      title: 'Test Card',
      view: 'timer',
      secondsLeft: 0
    })

    await nextTick()

    expect(
      Object.keys(wrapper.emitted()).includes('finish-counter')
    ).toBeTruthy()
  })

  it('should render error', async () => {
    const negativeIndex = createComponentFull({
      index: -1,
      currentStep: 0,
      title: 'Test Card',
      view: 'timer'
    })

    await nextTick()

    const indexError = negativeIndex.find('[data-test=active-error]')
    expect(indexError.exists()).toBeTruthy()

    const negativeStep = createComponentFull({
      index: 0,
      currentStep: -1,
      title: 'Test Card',
      view: 'timer'
    })

    await nextTick()

    const stepError = negativeIndex.find('[data-test=active-error]')
    expect(stepError.exists()).toBeTruthy()

    const negativeSeconds = createComponentFull({
      index: 0,
      currentStep: 0,
      title: 'Test Card',
      view: 'timer',
      secondsLeft: -1
    })

    await nextTick()

    const secondsError = negativeSeconds.find('[data-test=active-error]')
    expect(secondsError.exists()).toBeTruthy()
  })
})
