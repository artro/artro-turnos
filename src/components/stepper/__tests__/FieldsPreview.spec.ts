import { shallowMount } from '@vue/test-utils'
import FieldsPreview from '../card/views/FieldsPreview.vue'

type CardFieldProps = any

interface FieldsPreviewProps {
  stepNumber: number
  fields: CardFieldProps[]
}

describe.skip('Component: stepper/card/FieldsPreview', () => {
  const createComponent = (props: FieldsPreviewProps) =>
    shallowMount(FieldsPreview as any, { props })

  it('should render properly', () => {
    const wrapper = createComponent({
      stepNumber: 0,
      fields: []
    })

    expect(wrapper.exists()).toBeTruthy()
  })

  it('should create all fields', () => {
    const FIELDS = [
      { title: 'Test field', value: 'test' },
      { title: 'Test field', value: 'test2' },
      { title: 'Test field', value: '' }
    ]

    const wrapper = createComponent({
      stepNumber: 0,
      fields: FIELDS
    })

    const createdFields = wrapper.findAll('[data-test=field-preview]').length
    expect(createdFields).toBe(FIELDS.length)
  })

  it('should not render empty field', () => {
    const wrapper = createComponent({
      stepNumber: 0,
      fields: [{ title: 'Test field', value: '' }]
    })

    const createdField = wrapper.find('[data-test=field-preview]')
    expect(createdField.isVisible()).toBeFalsy()
  })
})
