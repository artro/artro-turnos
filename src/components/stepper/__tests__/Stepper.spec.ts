import { shallowMount } from '@vue/test-utils'
import Stepper from '../Stepper.vue'

type StepConfig<T> = any

describe.skip('Component: stepper/Stepper.vue', () => {
  const createComponent = (configs?: StepConfig<null>[]) =>
    shallowMount(Stepper as any, { props: { configs } })

  it('should render properly', () => {
    const wrapper = createComponent([
      {
        card: {
          title: 'Test Card',
          view: 'timer',
          secondsLeft: 5
        },
        view: {
          render: null,
          data: null
        }
      }
    ])

    expect(wrapper.exists()).toBeTruthy()
  })
})
