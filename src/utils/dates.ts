export interface MonthInfo {
  name: string
  daysCount: number
}

const MONTHS_DICT: MonthInfo[] = [
  {
    name: 'Enero',
    daysCount: 31
  },
  {
    name: 'Febrero',
    daysCount: 28
  },
  {
    name: 'Marzo',
    daysCount: 31
  },
  {
    name: 'Abril',
    daysCount: 30
  },
  {
    name: 'Mayo',
    daysCount: 31
  },
  {
    name: 'Junio',
    daysCount: 30
  },
  {
    name: 'Julio',
    daysCount: 31
  },
  {
    name: 'Agosto',
    daysCount: 31
  },
  {
    name: 'Septiembre',
    daysCount: 30
  },
  {
    name: 'Octubre',
    daysCount: 31
  },
  {
    name: 'Noviembre',
    daysCount: 30
  },
  {
    name: 'Diciembre',
    daysCount: 31
  }
]

export const WEEK_DAYS_NAMES = [
  'Domingo',
  'Lunes',
  'Martes',
  'Miércoles',
  'Jueves',
  'Viernes',
  'Sábado'
]

export const getMonthInfo = (year: number, month: number): MonthInfo =>
  isLeapYear(year) && month === 1
    ? {
        ...MONTHS_DICT[1],
        daysCount: 29
      }
    : MONTHS_DICT[month]

export const isLeapYear = (year: number) =>
  year % 400 === 0 || (year % 4 === 0 && year % 100 !== 0)

export const getWeekDay = (d: Util.SimpleDate) => {
  const obj = new Date(d.year, d.month, d.day)
  return WEEK_DAYS_NAMES[obj.getDay()]
}

export const getAbbreviation = (s: string) =>
  Array.from(s).shift()?.toUpperCase() || ''

export const generateRange = (start: number, length: number) => {
  const arr = []
  for (let i = 0; i < length; i++) arr.push(start + i)
  return arr
}

// == Only compares day, month and year
export const shallowDateComparision = (d1: Date, d2: Date): boolean =>
  d1.getDate() === d2.getDate() &&
  d1.getMonth() === d2.getMonth() &&
  d1.getFullYear() === d2.getFullYear()

// from January to December
export const getPrevMonth = (m: number) => (m - 1 > 0 ? m - 1 : 11)

// from December to January
export const getNextMonth = (m: number) => (m + 1) % 12

// day would be reseted
export const subractMonth = (d: Util.SimpleDate): Util.SimpleDate => {
  const month = getPrevMonth(d.month)

  return {
    day: 1,
    month,
    year: month === 11 ? d.year - 1 : d.year
  }
}

// day would be reseted
export const addMonth = (d: Util.SimpleDate): Util.SimpleDate => {
  const month = getNextMonth(d.month)

  return {
    day: 1,
    month,
    year: month === 0 ? d.year + 1 : d.year
  }
}

// converts date to: [día] de [mes] de [año]
export const dateToFullString = (d: Date) => {
  const day = String(d.getDate()).padStart(2, '0')
  const month = MONTHS_DICT[d.getMonth()].name
  const year = d.getFullYear()

  return `${day} de ${month} de ${year}`
}

// converts date to: DD/MM/YYYY
export const dateToSlashString = (d: Date) => {
  const day = String(d.getDate()).padStart(2, '0')
  const month = String(d.getMonth() + 1).padStart(2, '0')
  const year = d.getFullYear()

  return `${day}/${month}/${year}`
}
