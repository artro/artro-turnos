import { initializeApp } from 'firebase/app'
import { getAuth } from 'firebase/auth'

import type { FirebaseApp } from 'firebase/app'
import type { Auth, User } from 'firebase/auth'

export interface FirebaseConnectorConfig {
  apiKey?: string
  authDomain?: string
  databaseURL?: string
  projectId?: string
  storageBucket?: string
  messagingSenderId?: string
  appId?: string
  measurementId?: string
}

export class FirebaseConnector {
  config: FirebaseConnectorConfig
  app: FirebaseApp
  auth: Auth
  constructor(config: FirebaseConnectorConfig) {
    this.config = config
    this.app = initializeApp(this.config)
    this.auth = getAuth(this.app)
  }
}

export type { User }
