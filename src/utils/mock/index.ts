import ServiceTestIcon from '@/assets/icons/service-test-icon.svg'

const getById = <T>(group: any[], id: string) =>
  group.find((e) => e.id === id) as T

const getArrayByIds = <T>(group: any[], ids: string[]) =>
  group.filter((e) => ids.includes(e.id)) as T[]

const services: Service[] = [
  {
    id: 'traumatologia',
    name: 'Traumatología',
    icon: ServiceTestIcon,
    color: '#006EB7'
  },
  {
    id: 'apto-fisico',
    name: 'Apto Físico',
    icon: ServiceTestIcon,
    color: '#E83B4D'
  },
  {
    id: 'deportologia',
    name: 'Medicina del deporte',
    icon: ServiceTestIcon,
    color: '#F8AE04'
  }
]

export const subservices: Subservice[] = [
  {
    id: 'traumatologia-codo',
    name: 'Codo',
    service: getById<Service>(services, 'traumatologia')
  },
  {
    id: 'cardiologia-rodilla',
    name: 'Rodilla',
    service: getById<Service>(services, 'apto-fisico')
  },
  {
    id: 'deportologia-neumonologia-pediatrica',
    name: 'Neumonología pediátrica',
    service: getById<Service>(services, 'deportologia')
  },
  {
    id: 'traumatologia-columna',
    name: 'Columna',
    service: getById<Service>(services, 'traumatologia')
  }
]

export const locations: ClinicLocation[] = [
  {
    id: 'belgrano',
    name: 'Belgrano',
    address: 'asd 123',
    iframeLink:
      'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13125.11044654913!2d-58.68016209999999!3d-34.67294235000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bcbf903db234dd%3A0x6683e8120a7a5e1b!2sITUZAINGO%20GOLF%20CLUB!5e0!3m2!1ses-419!2sar!4v1654533148792!5m2!1ses-419!2sar'
  },
  {
    id: 'san-isidro',
    name: 'San Isidro',
    address: 'QWERT 1234',
    iframeLink:
      'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13125.11044654913!2d-58.68016209999999!3d-34.67294235000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bcbf903db234dd%3A0x6683e8120a7a5e1b!2sITUZAINGO%20GOLF%20CLUB!5e0!3m2!1ses-419!2sar!4v1654533148792!5m2!1ses-419!2sar'
  },

  {
    id: 'barrio-norte',
    name: 'Barrio Norte',
    address: 'qqqqq 555',
    iframeLink:
      'https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13125.11044654913!2d-58.68016209999999!3d-34.67294235000001!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95bcbf903db234dd%3A0x6683e8120a7a5e1b!2sITUZAINGO%20GOLF%20CLUB!5e0!3m2!1ses-419!2sar!4v1654533148792!5m2!1ses-419!2sar'
  },
  { id: 'nordelta', name: 'Nordelta' },
  { id: 'nunez', name: 'Nuñez' },
  { id: 'pilar', name: 'Pilar' },
  { id: 'lomas-de-zamora', name: 'Lomas de Zamora' }
]

export const professionals: Professional[] = [
  {
    id: '1',
    name: 'Tomas Gorkin',
    service: getById<Service>(services, 'traumatologia'),
    subservices: getArrayByIds<Subservice>(subservices, [
      'traumatologia-codo',
      'cardiologia-rodilla'
    ]),
    locations: getArrayByIds<ClinicLocation>(locations, [
      'barrio-norte',
      'belgrano'
    ]),
    availableDates: [
      {
        date: {
          year: 2022,
          month: 6,
          day: 17
        },
        times: [
          { hours: 8, minutes: 0 },
          { hours: 9, minutes: 0 },
          {
            hours: 11,
            minutes: 15
          }
        ],
        location: getById<ClinicLocation>(locations, 'barrio-norte'),
        subservice: getById<Subservice>(subservices, 'traumatologia-codo')
      },
      {
        date: {
          year: 2022,
          month: 6,
          day: 17
        },
        times: [
          { hours: 8, minutes: 0 },
          { hours: 11, minutes: 0 },
          { hours: 14, minutes: 0 }
        ],
        location: getById<ClinicLocation>(locations, 'pilar'),
        subservice: getById<Subservice>(subservices, 'traumatologia-codo')
      },
      {
        date: {
          year: 2022,
          month: 6,
          day: 18
        },
        times: [{ hours: 2, minutes: 0 }],
        location: getById<ClinicLocation>(locations, 'barrio-norte'),
        subservice: getById<Subservice>(subservices, 'traumatologia-codo')
      },
      {
        date: {
          year: 2022,
          month: 6,
          day: 20
        },
        times: [{ hours: 6, minutes: 0 }],
        location: getById<ClinicLocation>(locations, 'belgrano'),
        subservice: getById<Subservice>(subservices, 'cardiologia-rodilla')
      },
      {
        date: {
          year: 2022,
          month: 6,
          day: 23
        },
        times: [{ hours: 4, minutes: 0 }],
        location: getById<ClinicLocation>(locations, 'belgrano'),
        subservice: getById<Subservice>(subservices, 'cardiologia-rodilla')
      },
      {
        date: {
          year: 2022,
          month: 6,
          day: 25
        },
        times: [{ hours: 9, minutes: 0 }],
        location: getById<ClinicLocation>(locations, 'pilar'),
        subservice: getById<Subservice>(subservices, 'traumatologia-codo')
      },
      {
        date: {
          year: 2022,
          month: 6,
          day: 30
        },
        times: [{ hours: 1, minutes: 0 }],
        location: getById<ClinicLocation>(locations, 'lomas-de-zamora'),
        subservice: getById<Subservice>(subservices, 'cardiologia-rodilla')
      }
    ]
  },
  {
    id: '2',
    name: 'Juan Sanchez',
    service: getById<Service>(services, 'apto-fisico'),
    subservices: getArrayByIds<Subservice>(subservices, [
      'traumatologia-codo',
      'traumatologia-columna'
    ]),
    locations: getArrayByIds<ClinicLocation>(locations, [
      'barrio-norte',
      'belgrano'
    ]),
    availableDates: []
  },
  {
    id: '3',
    name: 'Santiago Maya',
    service: getById<Service>(services, 'deportologia'),
    subservices: getArrayByIds<Subservice>(subservices, [
      'deportologia-neumonologia-pediatrica'
    ]),
    locations: getArrayByIds<ClinicLocation>(locations, [
      'lomas-de-zamora',
      'san-isidro'
    ]),
    availableDates: []
  },
  {
    id: '4',
    name: 'William Buezas Madsen',
    service: getById<Service>(services, 'apto-fisico'),
    subservices: getArrayByIds<Subservice>(subservices, [
      'cardiologia-rodilla'
    ]),
    locations: getArrayByIds<ClinicLocation>(locations, [
      'nunez',
      'nordelta',
      'pilar'
    ]),
    availableDates: []
  }
]

export const insurances: Insurance[] = [
  {
    id: 'osde-365',
    name: 'OSDE 365'
  }
]

export const patients: Patient[] = [
  {
    id: '1',
    document: '11111111',
    name: 'Juan Saez',
    insurance: getById<Insurance>(insurances, 'osde-365'),
    birthDate: new Date(2000, 6, 19),
    affiliateNumber: '123456'
  },
  {
    id: '2',
    document: '22222222',
    name: 'William Madsen',
    insurance: getById<Insurance>(insurances, 'osde-365'),
    birthDate: new Date(2002, 9, 4),
    affiliateNumber: '456781'
  },
  {
    id: '3',
    document: '33333333',
    name: 'Tomas Gorkin',
    insurance: getById<Insurance>(insurances, 'osde-365'),
    birthDate: new Date(1992, 2, 21),
    affiliateNumber: '0123994'
  },
  {
    id: '4',
    document: '44444444',
    name: 'Juan Sanchez',
    insurance: getById<Insurance>(insurances, 'osde-365'),
    birthDate: new Date(1998, 11, 7),
    affiliateNumber: '3423982'
  }
]

export const externalPatients: ExternalPatient[] = [
  {
    id: '8',
    name: 'Juan Perez',
    document: '49582737',
    birthDate: new Date(2001, 10, 9)
  },
  {
    id: '9',
    name: 'Roberto Sasia',
    document: '26500912',
    birthDate: new Date(1998, 11, 21)
  },
  {
    id: '10',
    name: 'Abigail Montenegro',
    document: '38938645',
    birthDate: new Date(2022, 5, 10)
  },
  {
    id: '12',
    name: 'Rigoberto Freud',
    document: '9288221',
    birthDate: new Date(2022, 5, 10)
  }
]

export const appointment: Appointment = {
  date: {
    year: 2022,
    month: 6,
    day: 17
  },
  time: {
    hours: 13,
    minutes: 0
  },
  location: getById<ClinicLocation>(locations, 'san-isidro'),
  patient: getById<Patient>(patients, '1'),
  professional: getById<Professional>(professionals, '1'),
  subservice: getById<Subservice>(subservices, 'traumatologia-codo'),
  confirmationDate: new Date(2022, 6, 15, 15, 0, 0, 0),
  id: '4DyvgUAYQV'
}

export const user: User = {
  id: 'tUyfdUAuQs',
  email: 'juan.s@pow.lat',
  profile: {
    name: 'Juan Saez',
    address: 'Santa Fe 1141',
    city: 'Villa María',
    province: 'Córdoba',
    refererSource: '',
    secondaryEmail: '',
    picture: ServiceTestIcon
  },
  patient: getById<Patient>(patients, '1')
}
