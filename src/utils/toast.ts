export const messages: Component.Toast[] = [
  {
    id: 'reservation-success',
    type: 'success',
    title: 'Turno confirmado',
    body: `\
      Recibirá un mail de confirmación, revise su casilla.
      <br />
      <br />
      Luego de confirmar la reserva, podrá cancelar el turno hasta 24 horas antes`
  },
  {
    id: 'reservation-expired',
    type: 'error',
    title: 'Confirmación expirada',
    body: 'When you do something noble and beautiful and nobody noticed, do not be sad. For the sun every morning is a beautiful spectacle and yet most of the audience still sleeps.'
  },
  {
    id: 'only-location-selected',
    type: 'info',
    title: 'Falta completar un campo',
    body: `Para avanzar, debes seleccionar una especialidad y/o profesional`
  },
  {
    id: 'successful-patient-association',
    type: 'success',
    title: 'Paciente Asociado con Éxito',
    body: 'When you do something noble and beautiful and nobody noticed, do not be sad. For the sun every morning is a beautiful spectacle and yet most of the audience still sleeps.'
  },
  {
    id: 'no-patients-found',
    type: 'error',
    title: 'No pudimos encontrar tu búsqueda',
    body: 'When you do something noble and beautiful and nobody noticed, do not be sad. For the sun every morning is a beautiful spectacle and yet most of the audience still sleeps.'
  }
]
