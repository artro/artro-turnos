export const service: Service = {
  id: 'unknown-service',
  color: '#AAAAAA',
  name: 'Servicio desconocido'
}

export const insurance: Insurance = {
  id: 'unknown-insurance',
  name: 'Cobertura desconocida'
}

export const patient: Patient = {
  id: 'unknown-patient',
  affiliateNumber: '00000000',
  birthDate: new Date(),
  document: '00000000',
  insurance: insurance,
  name: 'Paciente desconocido'
}

export const subservice: Subservice = {
  id: 'unknown-subservice',
  name: 'Especialidad desconocida',
  service
}

export const professional: Professional = {
  id: 'unknown',
  name: 'Profesional desconocido',
  locations: [],
  service,
  subservices: [],
  availableDates: []
}

export const location: ClinicLocation = {
  id: 'unknown',
  name: 'Ubicación desconocida'
}

export const toastMessage: Component.Toast = {
  id: 'unknown',
  title: 'Código de mensaje desconocido',
  body: 'Se debía mostrar otro mensaje, pero no se encuentra.',
  type: 'error'
}

export const user: User = {
  id: 'unknown-user',
  email: 'unknown@unknown.com',
  patient,
  profile: {
    address: 'Dirección desconocida',
    city: 'Ciudad desconocida',
    name: 'Usuario desconocido',
    picture: '',
    province: 'Provincia desconocida',
    refererSource: '',
    secondaryEmail: 'unknown2@unknown.com'
  }
}
