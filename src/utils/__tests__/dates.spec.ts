import * as dates from '../dates'

describe('dates.ts', () => {
  describe('isLeapYear', () => {
    it.each([
      2004, 2008, 2012, 2016, 2020, 2024, 2028, 2032, 2036, 2040, 2044, 2048,
      2052, 2056, 2060, 2064, 2068, 2072, 2076, 2080, 2084, 2088, 2092, 2096
    ])('should return true', (y) => {
      const result = dates.isLeapYear(y)
      expect(result).toBeTruthy()
    })
  })
})
