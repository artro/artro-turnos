import type { util } from 'chai'

export const indexEntities = <T extends Util.EntityWithID>(
  entities: T[]
): Util.Indexed<T> => {
  // == Array for map constructor
  // 0 -> id
  // 1 -> Location

  const raw: [string, T][] = []
  for (const e of entities) raw.push([e.id, e])

  // == Make index and return

  const index: Util.Indexed<T> = new Map(raw)
  return index
}

export const formatDate = (
  d: Date | null // outputs to DD/MM/YY
) =>
  d === null
    ? ''
    : d.toLocaleString('es', { day: 'numeric' }).padStart(2, '0') +
      '/' +
      d.toLocaleString('es', { month: 'numeric' }).padStart(2, '0') +
      '/' +
      d.toLocaleString('es', { year: 'numeric' }).padStart(2, '0')

export const normalizeStr = (str: string) =>
  str.normalize('NFD').replace(/[\u0300-\u036f]/g, '')

export const simpleDateToDate = (d: Util.SimpleDate): Date =>
  new Date(d.year, d.month, d.day)

export const compareSimpleDates = (d1: Util.SimpleDate, d2?: Util.SimpleDate) =>
  d1.day === d2?.day && d1.month === d2?.month && d1.year === d2?.year

export const compareSimpleTimes = (t1: Util.SimpleTime, t2?: Util.SimpleTime) =>
  t1.hours === t2?.hours && t1.minutes === t2?.minutes

export const duplicate = <T>(o: object): T =>
  Object.fromEntries(Object.entries(o)) as T

export const collect = <T extends Util.EntityWithID>(
  index: Util.Indexed<T> | null
): T[] => Array.from(index?.values() || [])
