import {
  ApolloClient,
  InMemoryCache,
  HttpLink,
  type DocumentNode,
  type DefaultOptions
} from '@apollo/client/core'
import type { NormalizedCacheObject } from '@apollo/client/core'
import fetch from 'cross-fetch'

export interface GraphQLConnectorConfig {
  uri: string
  token: string
}

export class GraphQLConnector {
  private uri: string
  private token: string
  private client: ApolloClient<NormalizedCacheObject>

  constructor(uri: string, token: string) {
    this.uri = uri
    this.token = token

    this.client = this.makeApolloClient()
  }

  private makeLink() {
    return new HttpLink({
      uri: this.uri,
      credentials: 'include',
      headers: {
        Authorization: this.token
      },
      fetch //-> For compatibility on unit testing
    })
  }

  private makeCache() {
    return new InMemoryCache({
      addTypename: false
    })
  }

  private getDefaultOptions(): DefaultOptions {
    return {
      watchQuery: {
        fetchPolicy: 'no-cache',
        errorPolicy: 'ignore'
      },
      query: {
        fetchPolicy: 'no-cache',
        errorPolicy: 'all'
      },
      mutate: {
        fetchPolicy: 'no-cache',
        errorPolicy: 'all'
      }
    }
  }

  private makeApolloClient(): ApolloClient<NormalizedCacheObject> {
    const cache = this.makeCache()
    const link = this.makeLink()
    const defaultOptions = this.getDefaultOptions()

    return new ApolloClient({
      link,
      cache,
      defaultOptions
    })
  }

  // R -> Return type
  // L -> Input type
  async query<R = any, L = any>(
    query: DocumentNode,
    locals?: L
  ): Promise<R | null> {
    try {
      const response = await this.client.query({
        query,
        variables: locals
      })

      return Object.values(response.data).pop() as R
    } catch (e) {
      console.error('error on query', e)
      return null
    }
  }

  // R -> Return type
  // L -> Input type
  async mutation<R = any, L = any>(
    mutation: DocumentNode,
    locals?: L
  ): Promise<R | null> {
    try {
      const response = await this.client.mutate({
        mutation,
        variables: locals
      })

      return Object.values(response.data).pop() as R
    } catch (e) {
      console.error('error on mutation', e)
      return null
    }
  }
}
