import { createRouter, createWebHistory } from 'vue-router'
import Login from '@/views/Login.vue'
import Reservation from '@/views/Reservation.vue'
import Dashboard from '@/views/Dashboard.vue'
import Appointment from '@/views/Appointment.vue'
import AssociatePatient from '@/views/AssociatePatient.vue'
import CreatePatient from '@/views/CreatePatient.vue'
import Profile from '@/views/Profile.vue'
import Register from '@/views/Register.vue'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Dashboard
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/register',
      name: 'Register',
      component: Register
    },
    {
      path: '/reservation',
      name: 'Reservation',
      component: Reservation
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/appointment/:id',
      name: 'Appointment',
      component: Appointment,
      props: true
    },
    {
      path: '/associate-patient',
      name: 'Associate Patient',
      component: AssociatePatient
    },
    {
      path: '/create-patient',
      name: 'Create Patient',
      component: CreatePatient
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile
    },
    {
      path: '/help',
      name: 'Help',
      component: Profile
    }
  ]
})

export default router
