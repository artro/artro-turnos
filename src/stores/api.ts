import { defineStore } from 'pinia'

import mockLocations from '@/utils/mock/locations.json'
import mockSubservices from '@/utils/mock/subservices.json'
import mockProfessionals from '@/utils/mock/professionals.json'
import {
  appointment as mockAppointment,
  insurances as mockInsurances
} from '@/utils/mock'

import { indexEntities } from '@/utils/helpers'

type Indexed<T extends Util.EntityWithID> = Util.Indexed<T>

export interface APIState {
  locations: Indexed<ClinicLocation> | null
  subservices: Indexed<Subservice> | null
  professionals: Indexed<Professional> | null
  appointments: Indexed<Appointment> | null
  insurances: Indexed<Insurance> | null
}

export const useApiStore = defineStore('apiStore', {
  state: (): APIState => ({
    locations: null,
    subservices: null,
    professionals: null,
    appointments: null,
    insurances: null
  }),
  actions: {
    async init() {
      this.locations = await this.getIndexedLocations()
      this.subservices = await this.getIndexedSubservices()
      this.professionals = await this.getIndexedProfessionals()
      this.appointments = await this.getIndexedAppointments()
      this.insurances = await this.fetchInsurances()
    },

    async getIndexedSubservices(): Promise<Indexed<Subservice> | null> {
      if (Settings.MOCKING)
        return indexEntities(mockSubservices as Subservice[])

      // if not mocking
      // TODO: request from api

      return null
    },

    async getIndexedLocations(): Promise<Indexed<ClinicLocation> | null> {
      if (Settings.MOCKING)
        return indexEntities(mockLocations as ClinicLocation[])

      // if not mocking
      // TODO: request from api

      return null
    },

    async getIndexedProfessionals(): Promise<Indexed<Professional> | null> {
      if (Settings.MOCKING)
        return indexEntities(mockProfessionals as Professional[])

      // if not mocking
      // TODO: request from api

      return null
    },

    async getIndexedAppointments(): Promise<Indexed<Appointment> | null> {
      if (Settings.MOCKING) return indexEntities([mockAppointment])

      // if not mocking
      // TODO: request from api

      return null
    },

    async fetchInsurances(): Promise<Indexed<Insurance> | null> {
      if (Settings.MOCKING) return indexEntities(mockInsurances)

      // if not mocking
      // TODO: request from api

      return null
    }
  }
})
