import { defineStore } from 'pinia'

import { messages } from '@/utils/toast'
import { toastMessage as fbMessage } from '@/utils/fallback'
import { indexEntities } from '@/utils/helpers'

export interface ToastState {
  messages: Util.Indexed<Component.Toast> | null
  messagesStack: Component.Toast[]
  showId: number
}

export const useToastStore = defineStore('toast', {
  state: (): ToastState => ({
    messages: null,
    messagesStack: [],
    showId: 0
  }),
  actions: {
    init() {
      this.messages = indexEntities(messages)
      this.messagesStack = []
    },
    push(code: string) {
      const m = this.messages?.get(code) || fbMessage
      this.messagesStack.push(m)
    },
    pop() {
      return this.messagesStack.shift()
    },
    popAllStack() {
      const stack = JSON.parse(
        JSON.stringify(this.messagesStack)
      ) as Component.Toast[]
      this.messagesStack = []
      return stack
    },
    show() {
      this.showId += 1
    }
  }
})
