import { defineStore } from 'pinia'

import type { UpdateUserInput } from '@/api/users/types'
import { API_URI } from '@/api/users'
import { GET_USER_QUERY, UPDATE_USER_MUTATION } from '@/api/users/gql'

import { GraphQLConnector } from '@/utils/graphqlConnector'
import { user as mockUser } from '@/utils/mock'

import { useFirebaseStore } from './firebase'

interface PublicUserStoreRouter {
  publicRoutes: string[]
  todoEstaBienCallback: () => void
  unauthenticatedCallback: () => void
  loggedOutCallback: () => void
  loggedInCallback: () => void
  invalidCallback: () => void
  redirectCallback: () => void
}

interface PublicUserStoreState {
  router: PublicUserStoreRouter
  firstTime: boolean
  user?: User
}

export const usePublicUserStore = defineStore({
  id: 'publicUser',
  state: (): PublicUserStoreState => ({
    router: {
      publicRoutes: [],
      todoEstaBienCallback: () => {},
      unauthenticatedCallback: () => {},
      loggedOutCallback: () => {},
      loggedInCallback: () => {},
      invalidCallback: () => {},
      redirectCallback: () => {}
    },
    firstTime: true,
    user: undefined
  }),
  actions: {
    init(router: PublicUserStoreRouter) {
      if (Settings.MOCKING) {
        this.user = mockUser
        return
      }

      const firebaseStore = useFirebaseStore()
      this.router = router
      firebaseStore.init({
        onLogin: () => {
          this.router.loggedInCallback()
        },
        onLogout: () => {
          this.router.loggedOutCallback()
        },
        onUnauthenticated: () => {
          this.router.unauthenticatedCallback()
        },
        onChange: () => {}
      })
    },

    async login(email: string, password: string): Promise<'OK' | null> {
      if (Settings.MOCKING) {
        if (email === mockUser.email && password === 'juan123') {
          this.user = mockUser
          return 'OK'
        }

        return null
      }

      const firebaseStore = useFirebaseStore()
      return await firebaseStore.login(email, password)
    },

    async logout() {
      if (Settings.MOCKING) {
        this.user = undefined
        return
      }

      const firebaseStore = useFirebaseStore()
      return await firebaseStore.logout()
    },

    async makeGQLConnector() {
      const firebaseStore = useFirebaseStore()
      const token = await firebaseStore.getAuthToken()

      if (token === null) {
        console.warn(
          'firebase could not generate token, probably user is undefined'
        )
        return null
      }

      return new GraphQLConnector(API_URI, token)
    },

    async fetch(): Promise<User | null> {
      const gqlc = await this.makeGQLConnector()
      if (gqlc === null) return null

      return await gqlc.query<User>(GET_USER_QUERY)
    },

    async update(input: UpdateUserInput) {
      const gqlc = await this.makeGQLConnector()
      if (gqlc === null) return

      return await gqlc.mutation(UPDATE_USER_MUTATION, { input })
    }
  }
})
