import { defineStore } from 'pinia'

import { API_URI } from '@/api/patients'
import {
  ALL_PATIENTS_QUERY,
  CREATE_PATIENT,
  PATIENT_QUERY
} from '@/api/patients/gql'
import type { CreatePatientInput } from '@/api/patients/types'

import {
  patients as mockPatients,
  externalPatients as mockExternalPatients,
  insurances as mockInsurances
} from '@/utils/mock'

import { GraphQLConnector } from '@/utils/graphqlConnector'

import { useFirebaseStore } from './firebase'
import { indexEntities } from '@/utils/helpers'
import { nanoid } from 'nanoid'

type Indexed<T extends Util.EntityWithID> = Util.Indexed<T>

interface PublicPatientsStoreState {
  patients: Indexed<Patient> | null
  externalPatients: Indexed<ExternalPatient> | null
}

export const usePublicPatientsStore = defineStore({
  id: 'publicPatients',
  state: (): PublicPatientsStoreState => ({
    patients: null,
    externalPatients: null
  }),
  actions: {
    async init() {
      this.patients = await this.fetchIndexedPatients()
      this.externalPatients = await this.fetchIndexedExternalPatients()
    },

    async makeGQLConnector() {
      const firebaseStore = useFirebaseStore()
      const token = await firebaseStore.getAuthToken()

      if (token === null) {
        console.warn(
          'firebase could not generate token, probably user is undefined'
        )
        return null
      }

      return new GraphQLConnector(API_URI, token)
    },

    async fetchIndexedPatients(): Promise<Indexed<Patient> | null> {
      if (Settings.MOCKING) return indexEntities(mockPatients)

      const gqlConnector = await this.makeGQLConnector()
      if (gqlConnector === null) return null

      const patients = await gqlConnector.query<Patient[], any>(
        ALL_PATIENTS_QUERY
      )

      return patients === null ? null : indexEntities(patients)
    },

    async fetchIndexedExternalPatients(): Promise<Indexed<ExternalPatient> | null> {
      if (Settings.MOCKING) return indexEntities(mockExternalPatients)

      const gqlConnector = await this.makeGQLConnector()
      if (gqlConnector === null) return null

      const patients = await gqlConnector.query<Patient[], any>(
        ALL_PATIENTS_QUERY
      )

      return patients === null ? null : indexEntities(patients)
    },

    async fetch(id: string): Promise<Patient | null> {
      const gqlc = await this.makeGQLConnector()
      if (gqlc === null) return null

      const patient = await gqlc.query<Patient, any>(PATIENT_QUERY, { id })

      return patient
    },

    async create(input: CreatePatientInput) {
      if (Settings.MOCKING) {
        const newPatient: Patient = {
          ...input,
          id: nanoid(10),
          affiliateNumber: String(Math.floor(Math.random() * 10000000))
        }

        this.patients?.set(newPatient.id, newPatient)
        return
      }

      const gqlc = await this.makeGQLConnector()
      if (gqlc === null) return

      return await gqlc.mutation<any, CreatePatientInput>(CREATE_PATIENT, input)
    },

    async associate(input: ExternalPatient) {
      if (Settings.MOCKING) {
        const newPatient: Patient = {
          ...input,
          affiliateNumber: String(Math.floor(Math.random() * 10000000)),
          insurance: mockInsurances[0]
        }

        this.patients?.set(newPatient.id, newPatient)
        this.externalPatients?.delete(input.id)
        return
      }
    }
  }
})
