import { defineStore } from 'pinia'
import { FirebaseConnector } from '../utils/firebaseConnector'
import type { User } from '../utils/firebaseConnector'
import type { FirebaseConnectorConfig } from '../utils/firebaseConnector'
import { getIdToken, signInWithEmailAndPassword, signOut } from '@firebase/auth'

const firebaseConfig: FirebaseConnectorConfig = {
  apiKey: 'AIzaSyCA9oZrY_KIgn5QiiXSv2xYWdDQd6M1joY',
  authDomain: 'artro-dev.firebaseapp.com',
  projectId: 'artro-dev',
  storageBucket: 'artro-dev.appspot.com',
  messagingSenderId: '783559368072',
  appId: '1:783559368072:web:362c2bf2fbc2c68d472e3d',
  measurementId: 'G-V2MG84DX49'
}
interface FirebaseInitCallbacks {
  onLogin: (user: User) => void
  onLogout: () => void
  onChange: (user: User) => void
  onUnauthenticated: () => void
}

interface FirebaseStoreState {
  user?: User
  connector: FirebaseConnector
}

export const useFirebaseStore = defineStore({
  id: 'firebase',
  state: (): FirebaseStoreState => ({
    user: undefined,
    connector: new FirebaseConnector(firebaseConfig)
  }),
  getters: {},
  actions: {
    init({
      onLogin,
      onLogout,
      onChange,
      onUnauthenticated
    }: FirebaseInitCallbacks) {
      this.connector.auth.onAuthStateChanged(
        (user) => {
          if (user) {
            let shouldLogin = !this.user
            this.user = user

            if (shouldLogin) onLogin(user)
            else onChange(user)
          } else {
            this.user = undefined
            onUnauthenticated()
          }
        },
        (error) => {
          console.error(error)
          this.user = undefined
          onLogout()
        }
      )
    },

    async login(email: string, password: string): Promise<'OK' | null> {
      try {
        await signInWithEmailAndPassword(this.connector.auth, email, password)
      } catch (e: any) {
        console.error('error login', e)
        return null
      }
      return 'OK'
    },

    async logout() {
      return await signOut(this.connector.auth)
    },

    async getAuthToken(): Promise<string | null> {
      if (!this.user) return null
      return await getIdToken(this.user)
    }
  }
})
