import { nanoid } from 'nanoid'
import { defineStore } from 'pinia'
import { useApiStore } from './api'

import { appointment as mockAppointment } from '@/utils/mock'
import { duplicate } from '@/utils/helpers'

type PartialAppointment = Component.PartialAppointment

export interface AppointmentState {
  currentAppointment: PartialAppointment
}

const CLEAN_APPOINTMENT: PartialAppointment = Object.freeze({
  id: null,
  patient: null,
  subservice: null,
  location: null,
  date: null,
  time: null,
  professional: null,
  confirmationDate: undefined
})

const MOCK_RESERVATION: boolean = false && Settings.MOCKING

export const useAppointmentStore = defineStore({
  id: 'appointment',
  state: (): AppointmentState => ({
    currentAppointment:
      duplicate<Component.PartialAppointment>(CLEAN_APPOINTMENT)
  }),
  actions: {
    init() {
      if (MOCK_RESERVATION) this.currentAppointment = mockAppointment
    },
    clear() {
      for (const [k, v] of Object.entries(CLEAN_APPOINTMENT))
        (this.currentAppointment as any)[k] = v
    },
    confirmCurrentAppointment() {
      const apiStore = useApiStore()

      // == Fill current appointment with missing data

      const confirmed: Appointment = {
        ...this.currentAppointment,
        confirmationDate: new Date(),
        id: nanoid(10)
      } as Appointment

      // == Push appointment and clear

      apiStore.appointments?.set(confirmed.id, confirmed)
      this.clear()
    }
  },

  getters: {
    isComplete: (state) => {
      const required = ['patient', 'location', 'date', 'time', 'professional']
      const s = state.currentAppointment as any

      for (const r of required) {
        if (typeof s[r] === 'undefined' || s[r] === null) return false
      }

      return true
    }
  }
})
