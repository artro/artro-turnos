export const API_URI = 'https://api.artro.clients.pow.lat/users/graphql'

export interface UserProfile {
  name: string
  birthday: string
  address: string
  city: string
  province: string
  refererSource: string
  secondaryEmail: string
  picture: string
}

export interface User {
  id: string
  document: string
  email: string
  profile: UserProfile
  labelsId: string[]
}

export interface UpdateUserInput {
  name: string
}
