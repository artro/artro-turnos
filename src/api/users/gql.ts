import { gql } from '@apollo/client/core'

export const GET_USER_QUERY = gql`
  query {
    user {
      id
      name
    }
  }
`

export const UPDATE_USER_MUTATION = gql`
  mutation UpdateUser($input: UpdateUserInput!) {
    updateUser(input: $input) {
      id
      name
    }
  }
`
