import { gql } from '@apollo/client/core'

export const ALL_PATIENTS_QUERY = gql`
  query {
    availablePatients {
      id
      name
      document
    }
  }
`
export const PATIENT_QUERY = gql`
  query ($id: ID) {
    patient(id: $id) {
      id
      name
      document
    }
  }
`

export const CREATE_PATIENT = gql`
  mutation ($input: CreatePatientInput!) {
    createPatient(input: $input) {
      id
    }
  }
`
