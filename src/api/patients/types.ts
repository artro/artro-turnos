export interface APIPatient {
  id: string
  name: string
  document: string
}

export interface CreatePatientInput {
  name: string
  document: string
  birthDate: Date
  insurance: Insurance | null
}
